module Main where

import Lib

main :: IO ()
main = do
    masses <- lines <$> readFile "input.txt"

    print . sum $ map (moduleFuel' . read) masses
