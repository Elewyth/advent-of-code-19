import Test.Hspec
import Test.QuickCheck

import Lib

main :: IO ()
main = hspec $ do
    describe "moduleFuel" $ do
        it "returns 2 for 12" $
            moduleFuel 12 `shouldBe` 2

        it "returns 2 for 14" $
            moduleFuel 14 `shouldBe` 2

        it "returns 654 for 1969" $
            moduleFuel 1969 `shouldBe` 654

        it "returns 33583 for 100756" $
            moduleFuel 100756 `shouldBe` 33583

    describe "moduleFuel'" $ do
        it "returns 2 for 12" $
            moduleFuel' 12 `shouldBe` 2

        it "returns 966 for 1969" $
            moduleFuel' 1969 `shouldBe` 966

        it "returns 50346 for 100756" $
            moduleFuel' 100756 `shouldBe` 50346
