module Lib where

moduleFuel :: Int -> Int
moduleFuel = subtract 2 . (floor :: Double -> Int) . (/ 3) . fromIntegral

moduleFuel' :: Int -> Int
moduleFuel' x =
    let fuel = moduleFuel x
    in if fuel <= 0
        then 0
        else fuel + moduleFuel' fuel
