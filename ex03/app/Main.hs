module Main where

import Lib

main :: IO ()
main = do
    input <- lines <$> readFile "input.txt"

    let
        a = toPath $ readVecs $ head input
        b = toPath $ readVecs $ last input

        intersections = getIntersections a b

        mClosest = getClosestIntersection intersections
        mShortest = getShortestIntersection intersections a b

    case mClosest of
        Just (x,y) -> do
            print (x,y)
            print (abs x + abs y)

        Nothing ->
            putStrLn "No closest intersection found"

    case mShortest of
        Just (int, d) -> do
            print int
            print d

        Nothing ->
            putStrLn "No shortest intersection found"

