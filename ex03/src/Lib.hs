module Lib where

import Debug.Trace

import Data.List (delete)
import Data.List.Split
import Data.Maybe (mapMaybe)
import Data.Tuple.Extra

type Path = ((Int, Int), Vec)

type Vec = (Int, Int)


readVecs :: String -> [Vec]
readVecs =
    map readSingleVec . splitOn ","

readSingleVec :: String -> Vec
readSingleVec ('R':i) = (read i, 0)
readSingleVec ('U':i) = (0, read i)
readSingleVec ('L':i) = (- (read i), 0)
readSingleVec ('D':i) = (0, - (read i))

toPath :: [Vec] -> [Path]
toPath =
    foldl (\(p@((x0, y0), (x1, y1)):ps) vec ->
            ((x0+x1, y0+y1), vec):p:ps
        )
        [((0,0), (0,0))]

getIntersections :: [Path] -> [Path] -> [(Int, Int)]
getIntersections a =
    foldMap $ getIntersections_ a

getIntersections_ :: [Path] -> Path -> [(Int, Int)]
getIntersections_ paths seg =
    mapMaybe (getIntersection seg) paths

getIntersection :: Path -> Path -> Maybe (Int, Int)
getIntersection a b =
    let (ux, uy) = both fromIntegral $ snd a
        (ax, ay) = both fromIntegral $ fst a
        (vx, vy) = both fromIntegral $ snd b
        (bx, by) = both fromIntegral $ fst b
        s = (uy*(bx-ax) - ux*(by-ay)) / (ux*vy - uy*vx)
        t = if ux /= 0
                then (vx*s+bx-ax) / ux
                else (vy*s+by-ay) / uy
        ia = (ux*t+ax, uy*t+ay)
        ib = (vx*s+bx, vy*s+by)
    in
        if 0 <= s && s <= 1 && 0 <= t && t <= 1
            then Just $ both floor ia
            else Nothing

-- g = u*t+a
-- f = v*s+b
-- u*t+a = v*s+b
-- ux*t+ax = vx*s+bx
-- uy*t+ay = vy*s+by
-- t = (vy*s+by-ay)/uy
-- t = (vx*s+bx-ax)/ux
-- ux*((vy*s+by-ay)/uy)+ax = vx*s+bx
-- ux*(vy*s+by-ay)/uy = vx*s+bx-ax
-- ux*(vy*s+by-ay) = uy*vx*s + uy*(bx-ax)
-- ux*vy*s + ux*(by-ay) = uy*vx*s + uy*(bx-ax)
-- ux*vy*s - uy*vx*s = uy*(bx-ax) - ux*(by-ay)
-- s*(ux*vy - uy*vx) = uy*(bx-ax) - ux*(by-ay)
-- s = (uy*(bx-ax) - ux*(by-ay)) / (ux*vy - uy*vx)

getClosestIntersection :: [(Int, Int)] -> Maybe (Int, Int)
getClosestIntersection intersections =
    foldl (\mres int@(x,y) ->
            case mres of
                Nothing -> Just int
                Just res@(rx,ry) -> if abs x + abs y < abs rx + abs ry
                    then Just int
                    else Just res
        )
        Nothing
        $ delete (0,0) intersections

getShortestIntersection :: [(Int, Int)] -> [Path] -> [Path] -> Maybe ((Int, Int), Int)
getShortestIntersection intersections a b =
    foldl (\mr int -> do
        la <- getPathLength a int
        lb <- getPathLength b int
        let l = la + lb
        return $ case mr of
            Nothing -> (int, l)
            Just other@(otherInt, otherLength) ->
                if otherLength <= l
                    then other
                    else (int, l)
    ) Nothing intersections

getPathLength :: [Path] -> (Int, Int) -> Maybe Int
getPathLength paths point = getPathLength_ (reverse $ delete ((0,0), (0,0)) paths) point 0

getPathLength_ :: [Path] -> (Int, Int) -> Int -> Maybe Int
getPathLength_ [] (x,y) _ = Nothing
getPathLength_ (((x0, y0), (vx, vy)):ps) (x,y) d =
    let
        t :: Double
        t = if vx /= 0
                then fromIntegral (x - x0) / fromIntegral vx
                else fromIntegral (y - y0) / fromIntegral vy
        diff = if vx /= 0
                then floor (fromIntegral vy * t + fromIntegral y0) - y
                else floor (fromIntegral vx * t + fromIntegral x0) - x
    in if diff == 0 && 0 <= t && t <= 1
        then Just $ d + floor (fromIntegral (abs vx + abs vy) * t)
        else getPathLength_ ps (x,y) (d + abs vx + abs vy)

-- x = ux*t+ax
-- y = uy*t+ay
-- t = (x - ax) / ux
