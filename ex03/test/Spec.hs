import Test.Hspec
import Test.QuickCheck

import Lib

main :: IO ()
main = hspec $ do
    describe "readVecs" $
        it "should read comma-separated string of vectors" $
            readVecs "R8,U5,L5,D3" `shouldBe` [(8,0), (0,5), (-5,0), (0,-3)]

    describe "toPath" $
        it "should convert a list of vectors into a path" $
            toPath [(8,0), (0,5), (-5,0), (0,-3)]
                `shouldBe` reverse [((0,0), (0,0)), ((0,0), (8,0)), ((8,0), (0,5)), ((8,5), (-5,0)), ((3,5), (0,-3))]

    describe "getIntersection" $ do
        it "should return Nothing for parallel paths" $
            getIntersection ((0,0), (1,0)) ((0,1), (1,0)) `shouldBe` Nothing

        it "should return Nothing for separated paths" $
            getIntersection ((0,0), (0,-1)) ((0,1), (1,0)) `shouldBe` Nothing

        it "should return Just the intersection for intersecting paths" $
            getIntersection ((0,1), (3,0)) ((1,0), (0,3)) `shouldBe` Just (1,1)

    describe "getIntersections" $ do
        it "should return the intersection of two simple paths" $
            getIntersections
                [((1,0), (0,3)), ((0,0), (1,0)), ((0,0), (0,0))]
                [((0,1), (3,0)), ((0,0), (0,1)), ((0,0), (0,0))]
            `shouldBe`
                [(1,1), (0,0)]

        it "should return the intersections of two paths" $
            getIntersections
                (reverse [((0,0), (8,0)), ((8,0), (0,5)), ((8,5), (-5,0)), ((3,5), (0,-3))])
                (reverse [((0,0), (0,7)), ((0,7), (6,0)), ((6,7), (0,-4)), ((6,3), (-4,0))])
            `shouldBe`
                [(3, 3), (6, 5), (0,0)]

    describe "getClosestIntersection" $ do
        it "should return the single non-origin intersection" $
            getClosestIntersection [(1,1), (0,0)] `shouldBe` Just (1,1)

        it "should return the closes non-origin intersection" $
            getClosestIntersection [(1578,-1394),(1578,-1905),(163,-144),(163,0),(728,64),(728,0),(531,64),(531,0),(-788,-547),(-456,-484),(9,-144),(9,0)] `shouldBe` Just (9,0)

    describe "getPathLength" $ do
        it "should return Nothing for a point not on the path" $
            getPathLength [((1,0), (0,3)), ((0,0), (1,0)), ((0,0), (0,0))] (5, 5) `shouldBe` Nothing

        it "should return Just the path length" $
            getPathLength [((1,0), (0,3)), ((0,0), (1,0)), ((0,0), (0,0))] (1, 1) `shouldBe` Just 2
