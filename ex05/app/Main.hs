module Main where

import Debug.Trace

import Data.List.Split (splitOn)
import Data.Vector ((//))
import qualified Data.Vector as Vector

import Lib

main :: IO ()
main = do
    memory <- map read . splitOn "," <$> readFile "input.txt"

    print $ runDiagnostic memory 1
    print $ runDiagnostic memory 5


runDiagnostic :: [Int] -> Int -> [Int]
runDiagnostic memory =
    snd . processIntcode' memory

