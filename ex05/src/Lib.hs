module Lib where

import Data.Vector (Vector, fromList, toList, (!), (//))
import Debug.Trace

data ParamMode
    = Position
    | Immediate
    deriving (Eq, Show)

data Operation
    = Binary (Int -> Int -> Int) ParamMode ParamMode
    | Read
    | Write ParamMode
    | Jump Bool ParamMode ParamMode
    | Halt

processIntcode :: [Int] -> [Int]
processIntcode program =
    let
        (state, _, _) = processIntcode_ 0 (fromList program, 0, [])
    in
        toList state

processIntcode' :: [Int] -> Int -> ([Int], [Int])
processIntcode' program input =
    let
        (state, _, output) = processIntcode_ input (fromList program, 0, [])
    in
        (toList state, output)

processIntcode_ :: Int -> (Vector Int, Int, [Int]) -> (Vector Int, Int, [Int])
processIntcode_ input vals@(state, index, output) =
    case parseOpcode $ state ! index of
        Right (Binary op pm1 pm2) -> processIntcode_ input $ processOpcodeBi vals op pm1 pm2
        Right Read -> processIntcode_ input $ processOpcodeRead input vals
        Right (Write pm1) -> processIntcode_ input $ processOpcodeWrite vals pm1
        Right Halt -> vals
        Right (Jump onTrue pm1 pm2) -> processIntcode_ input $ processOpcodeJump onTrue vals pm1 pm2
        Left i -> error $ "unknown opcode " ++ show i

processOpcodeBi :: (Vector Int, Int, [Int]) -> (Int -> Int -> Int) -> ParamMode -> ParamMode -> (Vector Int, Int, [Int])
processOpcodeBi (state, index, output) op pm1 pm2 =
    let
        a = readParam state pm1 $ index + 1
        b = readParam state pm2 $ index + 2
    in
        (state // [ (state ! (index + 3), op a b) ], index + 4,  output)

processOpcodeRead :: Int -> (Vector Int, Int, [Int]) -> (Vector Int, Int, [Int])
processOpcodeRead input (state, index, output) =
    (state // [ (state ! (index + 1), input) ], index + 2, output)

processOpcodeWrite :: (Vector Int, Int, [Int]) -> ParamMode -> (Vector Int, Int, [Int])
processOpcodeWrite (state, index, output) pm1 =
    let
        a = case pm1 of
            Position -> state ! (state ! (index + 1))
            Immediate -> state ! (index + 1)
    in
        (state, index + 2, a : output)

processOpcodeJump :: Bool -> (Vector Int, Int, [Int]) -> ParamMode -> ParamMode -> (Vector Int, Int, [Int])
processOpcodeJump onTrue (state, index, output) pm1 pm2 =
    let
        a = readParam state pm1 $ index + 1
        b = readParam state pm2 $ index + 2
    in if (a /= 0) == onTrue
        then (state, b, output)
        else (state, index + 3, output)

readParam :: Vector Int -> ParamMode -> Int -> Int
readParam state pm index =
    case pm of
        Position -> state ! (state ! index)
        Immediate -> state ! index

parseOpcode :: Int -> Either Int Operation
parseOpcode i =
    let
        baseOp = i `mod` 100
        p1 = parseParamMode $ (i `mod` 1000) `div` 100
        p2 = parseParamMode $ (i `mod` 10000) `div` 1000

    in case (baseOp, p1, p2) of
        (1, Right pm1, Right pm2)  -> Right $ Binary (+) pm1 pm2
        (2, Right pm1, Right pm2)  -> Right $ Binary (*) pm1 pm2
        (3, _, _)  -> Right Read
        (4, Right pm1, _)  -> Right $ Write pm1
        (5, Right pm1, Right pm2) -> Right $ Jump True pm1 pm2
        (6, Right pm1, Right pm2) -> Right $ Jump False pm1 pm2
        (7, Right pm1, Right pm2) -> Right $ Binary (\a b -> if a < b then 1 else 0) pm1 pm2
        (8, Right pm1, Right pm2) -> Right $ Binary (\a b -> if a == b then 1 else 0) pm1 pm2
        (99, _, _) -> Right Halt
        _ -> Left i

parseParamMode :: Int -> Either Int ParamMode
parseParamMode 0 = Right Position
parseParamMode 1 = Right Immediate
parseParamMode i = Left i

compute :: [Int] -> Int -> Int -> Int
compute memory noun verb =
    let
        initialized = fromList memory // [(1, noun), (2, verb)]
        (state, _, _) = processIntcode_ 0 (initialized, 0, [])
    in
        state ! 0
