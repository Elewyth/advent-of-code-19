import Test.Hspec
import Test.QuickCheck

import Lib

main :: IO ()
main = hspec $ do
    describe "processIntcode" $ do
        it "processes Intcode 1 (sum)" $
            processIntcode [1, 0, 0, 0, 99] `shouldBe` [2, 0, 0, 0, 99]

        it "processes Intcode 2 (multiplication)" $
            processIntcode [2, 3, 0, 3, 99] `shouldBe` [2, 3, 0, 6, 99]

        it "processes Intcode 2 with large indices" $
            processIntcode [2, 4, 4, 5, 99, 0] `shouldBe` [2, 4, 4, 5, 99, 9801]

        it "processes complex intcodes" $
            processIntcode [1, 1, 1, 4, 99, 5, 6, 0, 99] `shouldBe` [30, 1, 1, 4, 2, 5, 6, 0, 99]

        it "processes Intcode 3 (read from Input)" $ property $
            \x -> processIntcode' [3, 0, 99] x == ([x, 0, 99], [])

        it "processes Intcode 4 (write to Output)" $ property $
            \x -> processIntcode' [4, 3, 99, x] 0 == ([4, 3, 99, x], [x])

        it "processses Intcodes 3 and 4 to write the input to output" $ property $
            \x -> processIntcode' [3, 0, 4, 0, 99] x == ([x, 0, 4, 0, 99], [x])

        it "processes Intcode 1 (sum) with first param in immediate mode" $ property $
            \x y -> processIntcode [101, x, 5, 0, 99, y] == [x + y, x, 5, 0, 99, y]

        it "processes Intcode 4 (write) in immediate mode" $ property $
            \x -> processIntcode' [104, x, 99] 0 == ([104, x, 99], [x])

        it "processes Intcode 5 (jump-if-true)" $
            processIntcode [1005, 0, 7, 1, 0, 0, 0, 99] `shouldBe` [1005, 0, 7, 1, 0, 0, 0, 99]

        it "processes Intcode 6 (jump-if-false)" $
            processIntcode [1006, 0, 7, 1, 0, 0, 0, 99] `shouldBe` [2012, 0, 7, 1, 0, 0, 0, 99]

        it "processes Intcode 7 (less than)" $
            processIntcode [1107, 1, 2, 0, 99] `shouldBe` [1, 1, 2, 0, 99]

        it "processes Intcode 8 (equals) for false" $
            processIntcode [1108, 1, 2, 0, 99] `shouldBe` [0, 1, 2, 0, 99]

        it "processes Intcode 8 (equals) for true" $ property $
            \x -> processIntcode [1108, x, x, 0, 99] == [1, x, x, 0, 99]

    describe "parseParamMode" $ do
        it "parses position mode" $
            parseParamMode 0 `shouldBe` Right Position

        it "parses immediate mode" $
            parseParamMode 1 `shouldBe` Right Immediate

        it "fails on unknown mode" $
            parseParamMode 2 `shouldBe` Left 2

    describe "compute" $
        it "returns 2 for 0 and 0" $
            compute [1, 0, 0, 0, 99] 0 0 `shouldBe` 2
