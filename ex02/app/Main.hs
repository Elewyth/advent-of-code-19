module Main where

import Debug.Trace

import Data.List.Split (splitOn)
import Data.Vector ((//))
import qualified Data.Vector as Vector

import Lib

main :: IO ()
main = do
    memory <- map read . splitOn "," <$> readFile "input.txt"

    print $ findInputs memory 19690720

findInputs :: [Int] -> Int -> (Int, Int)
findInputs memory output =
    findInputs_ memory output 0

findInputs_ :: [Int] -> Int -> Int -> (Int, Int)
findInputs_ memory output i =
    let
        (noun, verb) = i `divMod` 100
        result = (\r -> trace (show (noun,verb) ++ " -> " ++ show r) r) $ compute memory noun verb
    in
        if result == output
            then (noun, verb)
            else if noun > 99
                then error "No inputs produce the desired output"
                else findInputs_ memory output (i+1)
