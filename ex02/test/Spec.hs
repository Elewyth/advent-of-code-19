import Test.Hspec
import Test.QuickCheck

import Lib

main :: IO ()
main = hspec $ do
    describe "processIntcode" $ do
        it "processes Intcode 1 (sum)" $
            processIntcode [1, 0, 0, 0, 99] `shouldBe` [2, 0, 0, 0, 99]

        it "processes Intcode 2 (multiplication)" $
            processIntcode [2, 3, 0, 3, 99] `shouldBe` [2, 3, 0, 6, 99]

        it "processes Intcode 2 with large indices" $
            processIntcode [2, 4, 4, 5, 99, 0] `shouldBe` [2, 4, 4, 5, 99, 9801]

        it "processes complex intcodes" $
            processIntcode [1, 1, 1, 4, 99, 5, 6, 0, 99] `shouldBe` [30, 1, 1, 4, 2, 5, 6, 0, 99]

    describe "compute" $
        it "returns 2 for 0 and 0" $
            compute [1, 0, 0, 0, 99] 0 0 `shouldBe` 2
