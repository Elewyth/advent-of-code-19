module Lib where

import Data.Vector (Vector, fromList, toList, (!), (//))

processIntcode :: [Int] -> [Int]
processIntcode input =
    toList $ fst $ processIntcode_ (fromList input) 0

processIntcode_ :: Vector Int -> Int -> (Vector Int, Int)
processIntcode_ state index =
    case state ! index of
        1 -> uncurry processIntcode_ $ processOpcode state index (+)
        2 -> uncurry processIntcode_ $ processOpcode state index (*)
        99 -> (state, index)
        i -> error $ "unknown opcode " ++ show i

processOpcode :: Vector Int -> Int -> (Int -> Int -> Int) -> (Vector Int, Int)
processOpcode state index op =
    let
        a = state ! (state ! (index + 1))
        b = state ! (state ! (index + 2))
    in
        (state // [ (state ! (index + 3), op a b) ], index + 4)

compute :: [Int] -> Int -> Int -> Int
compute memory noun verb =
    let
        initialized = fromList memory // [(1, noun), (2, verb)]
    in
        fst (processIntcode_ initialized 0) ! 0
