import Test.Hspec
import Test.QuickCheck

import Lib

main :: IO ()
main = hspec $ do
    describe "isValidPassword" $ do
        it "returns False for too small password" $
            isValidPassword 1 `shouldBe` False

        it "returns False for too large password" $
            isValidPassword 9999999 `shouldBe` False

        it "returns False for no adjacents" $
            isValidPassword 234567 `shouldBe` False

        it "returns False for decreasing" $
            isValidPassword 234415 `shouldBe` False

        it "returns True for 222222" $
            isValidPassword 222222 `shouldBe` True

    describe "isValidPassword'" $ do
        it "returns False for too small password" $
            isValidPassword' 1 `shouldBe` False

        it "returns False for too large password" $
            isValidPassword' 9999999 `shouldBe` False

        it "returns False for no adjacents" $
            isValidPassword' 234567 `shouldBe` False

        it "returns False for decreasing" $
            isValidPassword' 234415 `shouldBe` False

        it "returns False for 222222" $
            isValidPassword' 222222 `shouldBe` False

        it "returns True for 222233" $
            isValidPassword' 222233 `shouldBe` True

    describe "hasAdjacent" $ do
        it "returns True for 11" $
            hasAdjacent 11 `shouldBe` True

        it "returns True for 111" $
            hasAdjacent 111 `shouldBe` True

        it "returns True for 1221" $
            hasAdjacent 1221 `shouldBe` True

        it "returns False for 1212" $
            hasAdjacent 1212 `shouldBe` False

    describe "isWeakIncreasing" $ do
        it "returns True for 1111" $
            isWeakIncreasing 1111 `shouldBe` True

        it "returns True for 12334" $
            isWeakIncreasing 12334 `shouldBe` True

        it "returns False for 12331" $
            isWeakIncreasing 12331 `shouldBe` False

    describe "hasDouble" $ do
        it "returns True for 11" $
            hasDouble 11 `shouldBe` True

        it "returns False for 111" $
            hasDouble 111 `shouldBe` False

        it "returns True for 1221" $
            hasDouble 1221 `shouldBe` True

        it "returns False for 1212" $
            hasDouble 1212 `shouldBe` False

        it "returns True for 11122" $
            hasDouble 11122 `shouldBe` True
