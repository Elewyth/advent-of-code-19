module Main where

import Lib

main :: IO ()
main = do
    let
        allValid = [x | x <- [lower..upper], isValidPassword x]
        newValid = [x | x <- [lower..upper], isValidPassword' x]

    print $ length allValid
    print $ length newValid

