module Lib where

import Debug.Trace

-- Hardcoded inputs
lower :: Int
lower = 136760

upper :: Int
upper = 595730

isValidPassword :: Int -> Bool
isValidPassword i =
    and
        [ lower <= i
        , i <= upper
        , hasAdjacent i
        , isWeakIncreasing i
        ]

isValidPassword' :: Int -> Bool
isValidPassword' i =
    isValidPassword i && hasDouble i

hasAdjacent :: Int -> Bool
hasAdjacent i =
    let
        hasAdjacent_ (a:b:ds) =
            a == b || hasAdjacent_ (b:ds)
        hasAdjacent_ _ = False

    in
        hasAdjacent_ $ show i

isWeakIncreasing :: Int -> Bool
isWeakIncreasing i =
    let
        isWeakIncreasing_ :: String -> Bool
        isWeakIncreasing_ (a:b:ds) =
            (read [a] :: Int) <= (read [b] :: Int) && isWeakIncreasing_ (b:ds)
        isWeakIncreasing_ _ = True

    in
        isWeakIncreasing_ $ show i

hasDouble :: Int -> Bool
hasDouble i =
    let
        f [] d = [(d, 1)]
        f (g@(p,c):gs) d =
            if p == d
                then (d, c+1):gs
                else (d, 1):g:gs

        groups = foldl f [] $ show i
    in
        not . null $ filter ((== 2) . snd) groups
