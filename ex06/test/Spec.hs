import qualified Data.Map as Map
import Test.Hspec
import Test.QuickCheck

import Lib

main :: IO ()
main = hspec $ do
    describe "parseOrbit" $
        it "parses a valid orbit" $
            parseOrbit "A)B" `shouldBe` ("A", "B")

    describe "parseOrbits" $ do
        it "returns a single direct orbit" $
            parseOrbits ["COM)A"] `shouldBe` Map.fromList
                [ ("COM", Nothing)
                , ("A", Just "COM")
                ]

        it "returns an indirect orbit" $
            parseOrbits ["COM)A", "A)B"] `shouldBe` Map.fromList
                [ ("COM", Nothing)
                , ("A", Just "COM")
                , ("B", Just "A")
                ]

    describe "orbitChecksum" $ do
        it "return 0 for only COM" $
            orbitChecksum (Map.fromList [("COM", Nothing)]) `shouldBe` 0

        it "returns 1 for a single direct orbit" $
            orbitChecksum (Map.fromList
                [ ("COM", Nothing)
                , ("A", Just "COM")
                ]) `shouldBe` 1

        it "returns 3 for an indirect orbit" $
            orbitChecksum (Map.fromList
                [ ("COM", Nothing)
                , ("A", Just "COM")
                , ("B", Just "A")
                ]) `shouldBe` 3

        it "returns 4 for a branching indirect orbit" $
            orbitChecksum (Map.fromList
                [ ("COM", Nothing)
                , ("A", Just "COM")
                , ("B", Just "A")
                , ("C", Just "A")
                ]) `shouldBe` 5

    describe "findCommonCenter" $ do
        it "returns COM for direct orbits" $
            findCommonCenter (Map.fromList
                [ ("COM", Nothing)
                , ("A", Just "COM")
                , ("B", Just "COM")
                ]) "A" "B" `shouldBe` "COM"

        it "returns direct parent for direct orbits" $
            findCommonCenter (Map.fromList
                [ ("COM", Nothing)
                , ("A", Just "COM")
                , ("B", Just "A")
                , ("C", Just "A")
                ]) "B" "C" `shouldBe` "A"

        it "returns indirect parent of indirect orbits" $
            findCommonCenter (Map.fromList
                [ ("COM", Nothing)
                , ("A", Just "COM")
                , ("B", Just "A")
                , ("C", Just "B")
                , ("D", Just "A")
                , ("E", Just "D")
                ]) "C" "E" `shouldBe` "A"

        it "returns parent if one is direct parent" $
            findCommonCenter (Map.fromList
                [ ("COM", Nothing)
                , ("A", Just "COM")
                , ("B", Just "A")
                ]) "A" "B" `shouldBe` "A"

    describe "getOrbitChain" $ do
        it "returns empty list for no orbits" $
            getOrbitChain (Map.fromList
                [ ("COM", Nothing)
                ]) "COM" `shouldBe` []

        it "returns a single entry for a direct orbit" $
            getOrbitChain (Map.fromList
                [ ("COM", Nothing)
                , ("A", Just "COM")
                ]) "A" `shouldBe` ["COM"]

        it "returns a chain for a nested orbit" $
            getOrbitChain (Map.fromList
                [ ("COM", Nothing)
                , ("A", Just "COM")
                , ("B", Just "A")
                , ("C", Just "B")
                ]) "C" `shouldBe` ["B", "A", "COM"]

    describe "countJumps" $ do
        it "returns 0 for same destination" $
            countJumps (Map.fromList
                [ ("COM", Nothing)
                ]) "COM" "COM" `shouldBe` 0

        it "returns 2 for same center" $
            countJumps (Map.fromList
                [ ("COM", Nothing)
                , ("A", Just "COM")
                , ("B", Just "COM")
                ]) "A" "B" `shouldBe` 2

        it "returns 1 for single jump" $
            countJumps (Map.fromList
                [ ("COM", Nothing)
                , ("A", Just "COM")
                , ("B", Just "A")
                ]) "A" "B" `shouldBe` 1
