module Lib where

import Data.List.Split (splitOn)
import qualified Data.Map as Map
import Debug.Trace

type OrbitMap = Map.Map String (Maybe String)

parseOrbits :: [String] -> OrbitMap
parseOrbits =
    foldl (\m o ->
        let
            (center, object) = parseOrbit o
        in
            Map.insertWith (\_ old -> old) center Nothing $
                Map.insert object (Just center) m
    ) Map.empty

parseOrbit :: String -> (String, String)
parseOrbit input =
    let
        [center,object] = splitOn ")" input
    in
        (center, object)

orbitChecksum :: OrbitMap -> Int
orbitChecksum orbitMap =
    Map.foldrWithKey (\object _ res ->
            countOrbits orbitMap object + res
        ) 0 orbitMap

countOrbits :: OrbitMap -> String -> Int
countOrbits orbitMap object =
    let
        mCenter = orbitMap Map.! object
    in
        case mCenter of
            Just center -> 1 + countOrbits orbitMap center
            Nothing -> 0

findCommonCenter :: OrbitMap -> String -> String -> String
findCommonCenter orbitMap a b =
    let
        aChain = getOrbitChain orbitMap a
        bChain = getOrbitChain orbitMap b
        mCenter = orbitMap Map.! b
    in if b `elem` aChain
        then b
        else if a `elem` bChain
            then a
            else case mCenter of
                Nothing -> "COM"
                Just center ->
                    findCommonCenter orbitMap a center


getOrbitChain :: OrbitMap -> String -> [String]
getOrbitChain orbitMap object =
    let
        mCenter = orbitMap Map.! object
    in case mCenter of
        Just center ->
            center : getOrbitChain orbitMap center
        Nothing -> []

countJumps :: OrbitMap -> String -> String -> Int
countJumps orbitMap a b =
    let
        commonCenter = findCommonCenter orbitMap a b
        maCenter = orbitMap Map.! a
        mbCenter = orbitMap Map.! b

        aJumpsToCenter = if a == commonCenter
            then 0
            else case maCenter of
                Just aCenter -> 1 + countJumps orbitMap aCenter commonCenter
                Nothing -> 1

        bJumpsToCenter = if b == commonCenter
            then 0
            else case mbCenter of
                Just bCenter -> 1 + countJumps orbitMap commonCenter bCenter
                Nothing -> 1
    in
        if a == b
            then 0
            else aJumpsToCenter + bJumpsToCenter
