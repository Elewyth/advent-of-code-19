module Main where

import qualified Data.Map as Map

import Lib

main :: IO ()
main = do
    orbitMap <- parseOrbits . lines <$> readFile "input.txt"

    print $ orbitChecksum orbitMap

    let (Just youCenter) = orbitMap Map.! "YOU"
        (Just sanCenter) = orbitMap Map.! "SAN"

    print $ countJumps orbitMap youCenter sanCenter

